import os
import cv2

from src.show.show_object import ShowObject
from src.detect.oid_object_detect import OidDetectUtils
# from src.detect.custom_obj_utils import ObjectDetectUtils
from utils.constant import DATA_DIR, EXT_IMGs


if __name__ == '__main__':
    shower = ShowObject()
    detector = OidDetectUtils()

    folder = os.path.join(DATA_DIR, "test")
    paths = [os.path.join(folder, fn) for fn in os.listdir(folder) if os.path.splitext(fn)[1].lower() in EXT_IMGs]

    result_dir = os.path.join(folder, "result")
    if not os.path.exists(result_dir):
        os.mkdir(result_dir)

    for path in paths:
        print(os.path.basename(path))
        _img = cv2.imread(path)
        dets = detector.detect(_img)
        result_img = shower.show_object(_img, dets)
        cv2.imwrite(os.path.join(result_dir, "result_" + os.path.split(path)[1]), result_img)
