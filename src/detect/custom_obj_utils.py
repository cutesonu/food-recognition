import os
import cv2
import sys
import numpy as np
import tensorflow as tf

from src.show.show_object import ShowObject
from utils.constant import CUSTOM_MODEL, LABELS, KEY_LABEL, KEY_COLOR, KEY_FRECT, KEY_SCORE, COLORS


os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'


class ObjectDetectUtils:
    def __init__(self):
        self.confidence = 0.4

        model_path = os.path.join(CUSTOM_MODEL + "/frozen_inference_graph.pb")
        if not os.path.exists(model_path):
            print("no exist ResNet model\n")
            sys.exit(1)
        else:
            self.path_to_ckpt = model_path
            self.__load_model()

    def __load_model(self):
        # Load model into memory
        print('loading ResNet model...')
        detection_graph = tf.Graph()
        with detection_graph.as_default():
            od_graph_def = tf.GraphDef()
            with tf.gfile.GFile(self.path_to_ckpt, 'rb') as fid:
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name='')

        print('configure ResNet model...')
        self.sess = None
        with detection_graph.as_default():
            self.sess = tf.Session(graph=detection_graph)
            self.image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
            self.detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
            self.detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
            self.detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')
            self.num_detections = detection_graph.get_tensor_by_name('num_detections:0')

    def detect(self, img):
        image_np_expanded = np.expand_dims(img, axis=0)

        (boxes, scores, classes, num) = self.sess.run(
            [self.detection_boxes, self.detection_scores, self.detection_classes, self.num_detections],
            feed_dict={self.image_tensor: image_np_expanded})

        objs = []
        rects = []
        for i in range(boxes.shape[1]):
            obj_id = int(classes[0][i])
            if scores[0][i] < self.confidence:
                continue

            y_min, x_min, y_max, x_max = boxes[0][i].tolist()
            rect = y_min, x_min, y_max, x_max

            cen_pt_x = (x_min + x_max) / 2
            cen_pt_y = (y_min + y_max) / 2

            duplicated = False
            for [x, y, x2, y2] in rects:
                if x < cen_pt_x < x2 and y < cen_pt_y < y2:
                    duplicated = True
                    break

            if not duplicated:
                rects.append(rect)
                objs.append({KEY_LABEL: LABELS[obj_id],
                             KEY_FRECT: rect,
                             KEY_SCORE: scores[0][i],
                             KEY_COLOR: COLORS[obj_id]
                             })

        return objs


if __name__ == '__main__':
    shower = ShowObject()

    folder = "../data/photos"
    fns = [os.path.join(folder, fn) for fn in os.listdir(folder) if os.path.splitext(fn)[1] == ".jpg"]

    import time
    start = time.time()
    detector = ObjectDetectUtils()
    step1 = time.time()
    cnt = 0
    total = 0
    for fn in fns:
        print(fn)
        _img = cv2.imread(fn)
        _objs = detector.detect(_img)
        show_img = shower.show_object(_img, _objs)
        cv2.imshow("img", show_img)
        cv2.waitKey(0)
        total += 1
        if len(_objs) > 0:
            cnt += 1

    step2 = time.time()
    print("loading model:", step1 - start)
    print("processing:", (step2 - step1) / cnt)
    print("total : {} , count : {}".format(total, cnt))

    # test_folder = "../test/test"
    # for folder_name in os.listdir(test_folder):
    #     if os.path.isdir(test_folder + "/" + folder_name):
    #         path = test_folder + "/" + folder_name
    #         for fn in os.listdir(path):
    #             img_path = path + "/" + fn
    #             os.rename(img_path, os.path.join(test_folder, fn))

    # ('loading model:', 4.1134490966796875)
    # ('processing:', 0.41421665943844216)
    # total : 555 , count : 426
