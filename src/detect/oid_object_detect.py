import os
import sys
import cv2
import numpy as np
import tensorflow as tf


from src.detect.label_map_utils import string_to_label_map
from utils.constant import OID_MODEL, OID_LABEL, OID_OBJECTS, KEY_LABEL, KEY_FRECT, KEY_SCORE, KEY_COLOR, \
    LABELS, COLORS


os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'


class OidDetectUtils:
    def __init__(self):
        self.target_objects = OID_OBJECTS

        self.confidence_thresh = 0.3
        self.height_ratio_thresh = 0.2

        label_dict_path = os.path.join(OID_LABEL, 'oid_bbox_trainable_label_map.pbtxt.txt')
        if not os.path.exists(label_dict_path):
            sys.stderr.write("no exist oid label data\n")
            sys.exit(1)
        else:
            self.label_dicts = self.__load_label_dict(dict_path=label_dict_path)

        model = os.path.join(OID_MODEL, "frozen_inference_graph.pb")
        if not os.path.exists(model):
            sys.stderr.write("no exist oid Faster-RCNN model\n")
            sys.exit(1)
        else:
            self.path_to_ckpt = model
            self.__load_model()

    @staticmethod
    def __load_label_dict(dict_path):
        with open(dict_path, 'r') as fp:
            str_label_map = fp.read()
            label_map_dicts = string_to_label_map(str_label_map)
            return label_map_dicts

    def __load_model(self):
        # Load model into memory
        print('loading model {}...'.format(os.path.split(self.path_to_ckpt)[-1]))
        detection_graph = tf.Graph()
        with detection_graph.as_default():
            od_graph_def = tf.GraphDef()
            with tf.gfile.GFile(self.path_to_ckpt, 'rb') as fid:
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name='')

        print('configure model...')
        self.sess = None
        with detection_graph.as_default():
            self.sess = tf.Session(graph=detection_graph)
            self.image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
            self.detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
            self.detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
            self.detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')
            self.num_detections = detection_graph.get_tensor_by_name('num_detections:0')
        print('initialized.')

    def detect(self, img):
        image_np_expanded = np.expand_dims(img, axis=0)

        (boxes, scores, classes, num) = self.sess.run(
            [self.detection_boxes, self.detection_scores, self.detection_classes, self.num_detections],
            feed_dict={self.image_tensor: image_np_expanded})

        objs = []
        first_obj = None
        for i in range(boxes.shape[1]):
            obj_id = int(classes[0][i])
            score = scores[0][i]
            y_min, x_min, y_max, x_max = boxes[0][i].tolist()
            rect = [x_min, y_min, x_max, y_max]

            if first_obj is None:
                first_obj = {
                    KEY_LABEL: self.label_dicts[obj_id - 1]["display_name"],
                    KEY_FRECT: rect,
                    KEY_SCORE: score,
                    KEY_COLOR: (0, 0, 0)
                }

            if score < self.confidence_thresh:
                continue
            if rect[3] - rect[1] < self.height_ratio_thresh:
                continue

            for label in self.target_objects.keys():
                idxs = self.target_objects[label]
                if obj_id in idxs:
                    display_name = self.label_dicts[obj_id - 1]["display_name"]
                    print("\t {}" + label)
                    objs.append({KEY_LABEL:  display_name,
                                 KEY_FRECT: rect,
                                 KEY_SCORE: scores[0][i],
                                 KEY_COLOR: COLORS[LABELS.index(label)]})

        if len(objs) == 0:
            objs.append(first_obj)
        return objs


if __name__ == '__main__':
    from src.show.show_object import ShowObject
    shower = ShowObject()
    detector = OidDetectUtils()

    folder = "../../data/test"
    fns = [os.path.join(folder, fn) for fn in os.listdir(folder) if os.path.splitext(fn)[1] == ".jpg"]
    for fn in fns:
        _img = cv2.imread(fn)
        print(fn)
        _objs = detector.detect(_img)
        res_img = shower.show_object(_img, _objs)
        cv2.imwrite(os.path.join(folder, "result_" + os.path.split(fn)[1]), res_img)
