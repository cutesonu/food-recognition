import cv2
import os
import json
import xmltodict

from utils.constant import DATA_DIR


def scan(folder, idx=0):
    fns = [fn for fn in os.listdir(folder) if os.path.splitext(fn)[1] in [".jpg"]]
    fns.sort()
    names = []
    for fn in fns:
        try:
            base = os.path.splitext(fn)[0]
            img_path = os.path.join(folder, fn)
            xml_path = os.path.join(folder, base + ".xml")

            json_objs = []

            with open(xml_path, 'r') as fp:
                xml_string = fp.read()
                xml_dict = xmltodict.parse(xml_string)
            img = cv2.imread(img_path)
            xml_objs = xml_dict['annotation']['object']

            if type(xml_objs) == list:
                for obj in xml_objs:
                    name = obj['name']
                    if name not in ['gun', 'hooded']:
                        continue
                    if name not in names:
                        names.append(name)
                    x1 = int(obj['bndbox']['xmin'])
                    x2 = int(obj['bndbox']['xmax'])
                    y1 = int(obj['bndbox']['ymin'])
                    y2 = int(obj['bndbox']['ymax'])

                    json_objs.append(
                        {
                            "bndbox": {
                                "xmin": x1,
                                "ymin": y1,
                                "xmax": x2,
                                "ymax": y2,
                            },
                            "name": name
                        }
                    )
                    cv2.rectangle(img, (x1, y1), (x2, y2), (0, 0, 255), 2)
            else:
                obj = xml_objs
                name = obj['name']
                if name not in ['gun', 'hooded']:
                    continue

                if name not in names:
                    names.append(name)
                x1 = int(obj['bndbox']['xmin'])
                x2 = int(obj['bndbox']['xmax'])
                y1 = int(obj['bndbox']['ymin'])
                y2 = int(obj['bndbox']['ymax'])

                json_objs.append(
                    {
                        "bndbox": {
                            "xmin": x1,
                            "ymin": y1,
                            "xmax": x2,
                            "ymax": y2,
                        },
                        "name": name
                    }
                )

                cv2.rectangle(img, (x1, y1), (x2, y2), (0, 0, 255), 2)

            cv2.imshow("img", img)
            cv2.waitKey(1)

            new_fn = str(idx) + "_" + os.path.split(img_path)[1]
            json_dict = {
                "filename": new_fn,
                "size": {
                    "width": img.shape[1],
                    "height": img.shape[0]
                },
                "object": json_objs
            }
            json_path = os.path.join("train_data/data/jsons/", os.path.splitext(new_fn)[0] + ".json")
            with open(json_path, 'w') as fp:
                json.dump(json_dict, fp)

            cv2.imwrite("train_data/data/images/" + new_fn, img)

        except Exception as e:
            print(fn + str(e))

    print(names)


def func1(folder):
    fns = [os.path.splitext(fn)[0] for fn in os.listdir(folder) if os.path.splitext(fn)[1] == ".jpg"]
    with open("trainval.txt", "w") as fp:
        for fn in fns:
            fp.write(fn + "\n")


def func2(folder):
    """
        {
            "object": [
                {"bndbox": {"xmin": 722, "ymin": 306, "ymax": 327, "xmax": 817}, "name": "NUM_L40"},
                {"bndbox": {"xmin": 981, "ymin": 296, "ymax": 322, "xmax": 1091}, "name": "NUM_50"}
            ],
            "filename": "512-L-sample_9.jpg",
            "size": {"width": 1200, "height": 675}
        }
    """
    fns = [fn for fn in os.listdir(folder) if os.path.splitext(fn)[1] == ".json"]
    for fn in fns:
        with open(os.path.join(folder, fn), "r") as fp:
            json_dict = json.load(fp)
            objs = json_dict["object"]
            for obj in objs:
                name = obj['name']
                if name not in ['hooded', 'gun']:
                    print(fn)


def standardize(folder):
    standard_height = 480
    paths = [os.path.join(folder, fn) for fn in os.listdir(folder)]
    paths.sort()
    for path in paths:
        if not os.path.isfile(path):
            continue
        img = cv2.imread(path)
        if img is None:
            continue
        ratio = standard_height / img.shape[0]
        img = cv2.resize(img, None, fx=ratio, fy=ratio)

        cv2.imwrite(path, img)


if __name__ == '__main__':
    # scan("train_data/Dataset", idx=1)
    # func1("train_data/data/images")
    # func2("train_data/data/jsons")
    standardize(folder=os.path.join(DATA_DIR, "test"))
