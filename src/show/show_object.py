import cv2
import numpy as np

from utils.constant import KEY_SCORE, KEY_FRECT, KEY_LABEL, KEY_COLOR


class ShowObject:
    def __init__(self):
        self.rect_color = (255, 255, 0)
        self.txt_color = (255, 255, 0)

    @staticmethod
    def show_object(img, results, show_width=700):
        ratio = show_width / img.shape[0]
        show_img = cv2.resize(img, None, fx=ratio,  fy=ratio)

        img_h, img_w = show_img.shape[:2]

        for result in results:
            (x, y, x2, y2) = (result[KEY_FRECT] * np.array([img_w, img_h, img_w, img_h])).astype(np.int)
            label = result[KEY_LABEL]
            score = float(result[KEY_SCORE])
            color = result[KEY_COLOR]

            str_label = "{}: {:.1f}%".format(label, score * 100)

            cv2.rectangle(show_img, (x, y), (x2, y2), color, 2)
            cv2.putText(show_img, str_label, (x, y + 30), cv2.FONT_HERSHEY_SIMPLEX, 0.7, color, 2)

        return show_img
