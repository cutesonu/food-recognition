import os
import numpy as np

# [ROOT] ===============================================================================================================
_cur_dir = os.path.dirname(os.path.realpath(__file__))
ROOT_DIR = os.path.join(_cur_dir, os.pardir)

DATA_DIR = os.path.join(ROOT_DIR, 'data')

LOG_DIR = os.path.join(ROOT_DIR, 'logs')
if not os.path.exists(LOG_DIR):
    os.mkdir(LOG_DIR)
MODEL_DIR = os.path.join(ROOT_DIR, 'model')

# MODELS ===============================================================================================================
####
OID_DIR = os.path.join(MODEL_DIR, "oid")
# OID_RESNET = os.path.join(OID_DIR + "faster_rcnn_inception_resnet_v2_atrous_oid_2018_01_28")
OID_MODEL = os.path.join(OID_DIR, "faster_rcnn_inception_resnet_v2_atrous_lowproposals_oid_2018_01_28")
OID_LABEL = os.path.join(OID_DIR, "oid_label_v4")

####
CUSTOM_DIR = os.path.join(MODEL_DIR, "custom")
CUSTOM_MODEL = os.path.join(CUSTOM_DIR, "output_inference_graph")


# [KEYS] ===============================================================================================================
KEY_FRECT = "float_rect"    # [x, y, x1, y1], range(float) = [0.0, 1.0], [0.0, 1.0]
KEY_COLOR = "color"
KEY_LABEL = "label"
KEY_SCORE = "score"
KEY_RECT = "rect"           # [x, y, w, h], range(int) = [0, img_width], [0, img_height]


EXT_IMGs = [".jpg", ".jpeg", ".png", ".tif"]
EXT_ANNO = ".json"


LABELS = ["Beer", "Burger", "Cocktail", "Coffee", "Pasta", "Salad", "Seafood", "Steak"]
np.random.seed(10101)
COLORS = [(np.random.randint(0, 255), np.random.randint(0, 255), np.random.randint(0, 255)) for i in range(len(LABELS))]

OID_OBJECTS = {
    "Beer": [84],
    "Burger": [270],
    "Cocktail": [161],
    "Coffee": [122, 149, 174, 472],
    "Pasta": [318],
    "Salad": [147],
    "Seafood": [142],
    "Steak": []
}
