#!/usr/bin/env bash

cd model/oid
wget http://download.tensorflow.org/models/object_detection/faster_rcnn_inception_resnet_v2_atrous_lowproposals_oid_2018_01_28.tar.gz
tar faster_rcnn_inception_resnet_v2_atrous_lowproposals_oid_2018_01_28.tar.gz

wget http://download.tensorflow.org/models/object_detection/faster_rcnn_inception_resnet_v2_atrous_oid_2018_01_28.tar.gz
tar faster_rcnn_inception_resnet_v2_atrous_oid_2018_01_28.tar.gz
